import gzip
from mysql import connector
import secret
from createFields import create_fields

database = connector.connect(
  host=secret.host,
  user=secret.user,
  passwd=secret.password,
  database=secret.database)
sql = "INSERT INTO `queries-B` (id, epoch, malicious, counter, frequency, lenght, entropy, domain) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

fields_dict = {}

databaseCursor = database.cursor()

errors = ['FAIL.SERVER-FAILURE.in-addr.arpa','FAIL.NON-AUTHORITATIVE.in-addr.arpa', 'localhost']

extract = """1471414671	52.95.30.39	FAIL.NON-AUTHORITATIVE.in-addr.arpa
1471414671	66.109.52.110	66-109-52-110.tvc-ip.com
1471414671	80.81.64.26	FAIL.NON-AUTHORITATIVE.in-addr.arpa
1471414671	213.164.30.97	FAIL.NON-AUTHORITATIVE.in-addr.arpa
1471414671	68.85.80.134	te-18-10-cdn01.germantowne.md.bad.comcast.net
1471414671	24.58.35.160	agg33.ithcnycy02r.northeast.rr.com
1471414671	196.32.210.41	so5-0-1-ua-ke-nbo1-01.ubuntunet.net
1471414671	207.248.97.54	FAIL.NON-AUTHORITATIVE.in-addr.arpa
1471414671	69.85.197.97	noc-core-02.mob.southernlightfiber.com
1471414671	24.58.35.161	FAIL.NON-AUTHORITATIVE.in-addr.arpa
1471414671	221.242.229.42	221x242x229x42.ap221.ftth.ucom.ne.jp
1471414675	67.251.102.224	cpe-67-251-102-224.stny.res.rr.com
1471414671	208.36.228.161	208.36.228.161.ptr.us.xo.net
1471414671	69.196.136.140	ae0-2140-lns02-tor2.teksavvy.com
1471414681	67.251.102.224	cpe-67-251-102-224.stny.res.rr.com
1471414671	183.207.195.145	145.195.207.183.static.js.chinamobile
1471414671	69.85.244.180	host-69-85-244-180.southernlightfiber.com
1471414671	118.217.103.71	FAIL.NON-AUTHORITATIVE.in-addr.arpa"""

up = '\x1b[1A'
cl = '\x1b[2K'
er = up + cl #up arrequest + clear line

days = ['5']

with open('int.txt', 'r') as readID:
    ID = int(readID.read())

#day = 'dns-names.l7.20160817.txt.gz'
if not days:
    print(er+'Nothing to be done.')
    print('It seems all files had been read already.')

for day in days:
    print(er+'Uploading ' + day + ' to MariaDB.')
    values = []
    for ii, request in enumerate(extract.split('\n')):
        if (ii+1) % 2 == 0:
            if ii < 2:
                print(str(ii+1) +" entries read.")
            if ii > 2:
                print(er+str(ii+1) +" entries read.")
            #print(values)
            #databaseCursor.executemany(sql, (values))
            #values = []

        fields = request.split('\n')[0].split('\t')
        if not fields[2] in errors:
            epoch = int(fields[0])
            malicious = 0
            domain = fields[2]
            counter, frequency, lenght, entropy = create_fields(fields_dict,domain,epoch)
            values.append((ID,epoch,malicious,counter,frequency,lenght,entropy,domain.encode('ascii')))
            ID += 1
    print(values)
    databaseCursor.executemany(sql, values)
    database.commit()
    with open('uploaded.txt', 'a') as uploadedFiles:
        uploadedFiles.write(day + '\n')
    with open('int.txt', 'w') as idsave:
        idsave.write(str(id))
    exit()
print(er,end='')
