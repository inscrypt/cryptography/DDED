# create a ascendant sorted list of the to ten million accessed domains and send it to mysql

import csv
import secret
from mysql import connector

sql = "INSERT INTO `mostAccessedDomainsAscending` (id, domain) VALUES (%s, %s)"

database = connector.connect(
  host=secret.host,
  user=secret.user,
  passwd=secret.password,
  database=secret.database)

with open('../Datasets/top10milliondomains.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    databaseCursor = database.cursor()
    values = []
    for ii, row in enumerate(reversed(list(readCSV))):
        if ii % 100000 == 0:
            print(ii, end='\r')
            databaseCursor.executemany(sql, values)
            values = []
        domain = row[1]
        values.append((ii,domain))
    databaseCursor.executemany(sql, values)
    database.commit()
