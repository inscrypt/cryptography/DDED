import gzip
from operator import itemgetter

#day = 'dns-names.l7.20160817.txt.gz'

up = '\x1b[1A'
cl = '\x1b[2K'
er = up + cl #up arrequest + clear line

dit = {}
days = [1]
for day in days:
    with gzip.open('../Datasets/dns-names.l7.20160817.txt.gz', 'rb') as requests:
        for ii, request in enumerate(requests):
            if (ii+1) % 1000 == 0:
                if ii < 1001:
                    print(str(ii+1) +" entries read.")
                if ii > 1000:
                    print(er+str(ii+1) +" entries read.")

            fields = request.decode('ascii').split('\n')[0].split('\t')
            epoch = int(fields[0])
            malicious = 0
            domain = fields[2]
            if domain in dit:
                dit[domain]+=1
            else:
                dit[domain]=1
    for key, value in sorted(dit.items(), key = itemgetter(1), reverse = False):
        if value > 100:
            print(key, value)
    exit()
