# -*- coding: utf-8 -*-
"""Example of using Isolation Forest for outlier detection
"""
# Author: Yue Zhao <zhaoy@cmu.edu>
# License: BSD 2 clause

from __future__ import division
from __future__ import print_function

import os
import sys
import numpy as np

# temporary solution for relative imports in case pyod is not installed
# if pyod is installed, no need to use the following line
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname("__file__"), '..')))

from pyod.models.iforest import IForest
from pyod.utils.data import generate_data

from pyod.utils.data import evaluate_print
from pyod.utils.example import visualize

def main():
	contamination = 0.1  # percentage of outliers
    n_train = 0.9  # number of training points
    n_test = 0.1  # number of testing points

    """
    # Generate sample data
    X_train, y_train, X_test, y_test = \
        generate_data(n_train=n_train,
                      n_test=n_test,
                      n_features=2,
                      contamination=contamination,
                      random_state=42)
    """

    ground, malicious = open('ground.csv', 'r'), open('malicious.csv', 'r')
    X_train, y_train, X_test, y_test = [], [], [], []
    n_ground, n_malicious = 750, 250

    ground.readline()
    lines = ground.readlines()
    for ii in range(n_ground):
        columns = lines[ii].split('\t')
        if ii < n_ground*n_train:
            X_train.append([columns[1],columns[2]])#,columns[2],columns[3]])
            y_train.append(0)
        else:
            X_test.append([columns[1],columns[2]])#,columns[2],columns[3]])
            y_test.append(0)

    malicious.readline()
    lines = malicious.readlines()
    for ii in range(n_malicious):
        columns = lines[ii].split('\t')
        if ii < n_malicious*n_train:
            X_train.append([columns[1],columns[2]])#,columns[2],columns[3]])
            y_train.append(1)
        else:
            X_test.append([columns[1],columns[2]])#,columns[2],columns[3]])
            y_test.append(1)

    X_train = np.array(X_train, np.float)
    y_train = np.array(y_train, np.int32)

    X_test = np.array(X_test, np.float)
    y_test = np.array(y_test, np.int32)

    print(X_train)
    #exit()

    # train IForest detector
    name = 'IForest'
    iforest = IForest()
    iforest.fit(X_train)

    # get the prediction labels and outlier scores of the training data
    y_train_pred = iforest.labels_  # binary labels (0: inliers, 1: outliers)
    y_train_scores = iforest.decision_scores_  # raw outlier scores

    # get the prediction on the test data
    y_test_pred = iforest.predict(X_test)  # outlier labels (0 or 1)
    y_test_scores = iforest.decision_function(X_test)  # outlier scores

    # evaluate and print the results
    print("\nOn Training Data:")
    evaluate_print(name, y_train, y_train_scores)
    
    print("\nOn Test Data:")
    evaluate_print(name, y_test, y_test_scores)

    # visualize the results
    visualize(name, X_train, y_train, X_test, y_test, y_train_pred, y_test_pred, show_figure=True, save_figure=False)

if __name__ == "__main__":
	main()