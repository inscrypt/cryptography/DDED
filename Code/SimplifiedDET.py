from zlib import compress, decompress
import random
import os
import hashlib
from createFields import create_fields
from random import randint, sample as randsample
from Crypto.Util.py3compat import bchr

from numpy.random import normal as randnorm, exponential
import string
from mysql import connector
import secret
#DET
import tempfile
from io import StringIO, BytesIO
from Crypto.Cipher import AES
#Resonably beautiful output
from printclean import printup

fields_dict = {}

sqlC = "SELECT content FROM cablegate WHERE id = "
sqlA = "SELECT domain FROM mostAccessedDomainsAscending WHERE id = "
sqlM = "INSERT INTO `queries-E` \
(id, epoch, malicious, counter, frequency, \
lenght, entropy, idfirst, idnext, iddoc, domain) \
VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

def main():
    print()
    cursor, database = DatabaseConnect()
    cableID = GetNextCableID(cursor)
    cable = GetCable(cableID, cursor)
    DNSqueries = []
    while cable != None:
        domain = GetDomain(cursor)
        DNSqueries += Exfiltrate(cable, domain)
        cable = GetCable(cable[0]+1, cursor)
    if DNSqueries != []:
        SaveDNSQueries(cursor, DNSqueries)
    else:
        printup("Nothing done")

def DatabaseConnect():
    printup('Connecting to database')
    try:
        database = connector.connect(
            host=secret.host,
            user=secret.user,
            passwd=secret.password,
            database=secret.database)
    except Exception as e:
        printup("Database connection error, exiting...")
        print(e)
        exit()
    printup("Database connected")
    print()
    cursor = database.cursor()
    return cursor, database

def GetNextCableID(cursor):
    printup("Quering for next cable id to exfiltrate")
    cursor.execute("SELECT MAX(idcable) FROM `queries-E`")
    ID = cursor.fetchone()[0]
    if ID == None:
        ID = 1
    else:
        ID += 1
    printup("Starting to exfiltrate from cable", ID)
    print()
    return ID

def GetCable(ID, cablegate):
    cablegate.execute(sqlC + str(ID))
    cable = cablegate.fetchone()[0]
    while cable == None:
        ID += 1
        cablegate.execute(sqlC + str(ID))
        cable = cablegate.fetchone()[0]
    return ID, cable

def GetDomain(alexa):
    domain = None
    while domain == None:
         ID = int(exponential(100000))
         alexa.execute(sqlA+str(ID))
         domain = alexa.fetchone()
    return ID, domain[0]

def Exfiltrate(cable, domain):
    maxSize = DNSSize(len(domain[1]))
    startTime = randint(1204329607, 1533513599)
    maxTime = randint(1, 60)
    minB = randint(1, 100)
    maxB = randint(minB, 100)
    comp = randint(0, 1)
    KEY = ''.join(randsample(string.ascii_letters + string.digits, 12))
    jobid = ''.join(randsample(string.ascii_letters + string.digits, 7))
    b = []
    b += run(cable, jobid, domain, startTime, KEY, maxSize, maxTime,
        minB, maxB, comp)
    return b

def DNSSize(domainSize):
    size = int(randnorm(21.272925457550368 - (domainSize + 1 + 8), 8.209950189039446))
    while size + domainSize + 1 + 8 > 253 or size < 0:
        size = int(randnorm(21.272925457550368 - (domainSize + 1 + 8), 8.209950189039446))
    return size

def SaveDNSQueries(sent, queries):
    preparing = "Preparing to send malicious requests: "
    printup(preparing + "sorting")
    queries.sort()
    printup(preparing + "adding id field")
    sent.execute("SELECT MAX(id) FROM `queries-M`")
    ID = sent.fetchone()
    print(ID)
    exit()
    #ID = sent.fetchone()[0] + 1
    for ii in range(len(a)):
        queries[ii] = [ID + ii] + queries[ii]
    printup("Sending requests")
    sent.executemany(sqlM, queries)
    sent.commit()

#support
#https://github.com/suminb/base62/blob/develop/base62.py
CHARSET_DEFAULT = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
string_types = (str,)
bytes_types = (bytes,)

def bytes_to_int(barray, byteorder="big", signed=False):
    """Converts a byte array to an integer value.
    Python 3 comes with a built-in function to do this, but we would like to
    keep our code Python 2 compatible.
    """

    try:
        return int.from_bytes(barray, byteorder, signed=signed)
    except AttributeError:
        # For Python 2.x
        if byteorder != "big" or signed:
            raise NotImplementedError()

        # NOTE: This won't work if a generator is given
        n = len(barray)
        ds = (x << (8 * (n - 1 - i)) for i, x in enumerate(bytearray(barray)))

        return sum(ds)

def _check_type(value, expected_type):
    """Checks if the input is in an appropriate type."""

    if not isinstance(value, expected_type):
        msg = "Expected {} object, not {}".format(
            expected_type, value.__class__.__name__
        )
        raise TypeError(msg)

def encodebytes(barray, charset=CHARSET_DEFAULT):
    """Encodes a bytestring into a base62 string.
    :param barray: A byte array
    :type barray: bytes
    :rtype: str
    """

    _check_type(barray, bytes_types)
    return encode(bytes_to_int(barray), charset=charset, BASE=62)

#support
def encode(n, charset, BASE, minlen=1):
    """Encodes a given integer ``n``."""

    chs = []
    while n > 0:
        r = n % BASE
        n //= BASE

        chs.append(charset[r])

    if len(chs) > 0:
        chs.reverse()
    else:
        chs.append("0")

    s = "".join(chs)
    s = charset[0] * max(minlen - len(s), 0) + s
    return s

#det.py
# Do a md5sum of the file
def md5(f):
    hash = hashlib.md5()
    ii = 0
    for chunk in iter(lambda: f.read(4096), ""):
        ii+=1
        if ii % 10000 == 0:
            print(ii, end='\r')
        hash.update(chunk.encode('UTF-8'))
    return hash.hexdigest()

#support
"""def pad(s):
    padsize = AES.block_size - len(s) % AES.block_size
    print(type(chr))
    return (s + padsize * bytes(padsize))"""
def pad(data_to_pad, block_size, style='pkcs7'):
    """Apply standard padding.
    :Parameters:
      data_to_pad : byte string
        The data that needs to be padded.
      block_size : integer
        The block boundary to use for padding. The output length is guaranteed
        to be a multiple of ``block_size``.
      style : string
        Padding algorithm. It can be *'pkcs7'* (default), *'iso7816'* or *'x923'*.
    :Return:
      The original data with the appropriate padding added at the end.
    """
    padding_len = block_size-len(data_to_pad)%block_size
    if style == 'pkcs7':
        padding = bchr(padding_len)*padding_len
    elif style == 'x923':
        padding = bchr(0)*(padding_len-1) + bchr(padding_len)
    elif style == 'iso7816':
        padding = bchr(128) + bchr(0)*(padding_len-1)
    else:
        raise ValueError("Unknown padding style")
    return data_to_pad + padding

#det.py
# http://stackoverflow.com/questions/12524994/encrypt-decrypt-using-pycrypto-aes-256
def aes_encrypt(message, key):
    messageE = message
    # Generate random CBC IV
    iv = os.urandom(AES.block_size)
    # Derive AES key from passphrase
    aes = AES.new(hashlib.sha256(key.encode('UTF-8')).digest(), AES.MODE_CBC, iv)

    # Return data size, iv and encrypted message
    return iv + aes.encrypt(pad(messageE,16))

#dns.py
#Send data over multiple labels (RFC 1034)
#Max query is 253 characters long (textual representation)
#Max label length is 63 bytes
def send(data, TLD, epoch, KEY, jobid, cableID, maxSize):
    domainID = TLD[0]
    TLD = TLD[1]

    data1 = encodebytes(data.encode('ascii'))

    #Calculate the remaining length available for our payload
    rem = 253 - len(TLD) - 1
    #Number of 63 bytes labels
    no_labels = rem // 64 #( 63 + len('.') )
    #Length of the last remaining label
    last_label_len = (rem % 64) - 1

    domains = []
    while data1 != "":
        size = randint(0,maxSize)
        data = data1[:size]
        data1 = data1[size:]
        while data != "":
            domain = ""
            data = str(jobid) + data
            for i in range(0, no_labels):
                if data == "": break
                label = data[:63]
                data = data[63:]
                domain += label + '.'
            if data == "":
                domain += TLD
            else:
                if last_label_len < 1:
                    domain += TLD
                else:
                    label = data[:last_label_len]
                    data = data[last_label_len:]
                    domain += label + '.' + TLD
            counter, frequency, length, entropy = create_fields(fields_dict,domain,epoch)
            print(length, domain)
            #domains.append((epoch,malicious,counter,frequency,length,entropy,domain))
    return domains

#det.py
#(id, epoch, malicious, counter, frequency, lenght, entropy, idfirst, idnext, iddoc, domain)
#   run(cable, jobid, domain, startTime, KEY)
def run(file_content, jobid, TLD, start_epoch, KEY, maxSize,
        MAX_TIME_SLEEP, MIN_BYTES_READ, MAX_BYTES_READ, COMPRESSION):
    cableID = file_content[0]
    file_content = file_content[1]
    """# checksum
    with open(file_to_send, 'rb') as f:
        file_content = f.read()"""
    buf = StringIO(file_content)
    #e = StringIO(file_content)
    e = BytesIO(file_content.encode('UTF-8'))
    checksum = md5(buf)
    del file_content
    epoch = start_epoch

    data = "%s|!|REGISTER|!|%s" % (
        jobid, checksum)
    #(id, epoch, malicious, counter, frequency, lenght,
    # entropy, idfirst, idnext, iddoc, domain)
    domains = send(data, TLD, epoch, KEY, jobid, cableID, maxSize)

    time_to_sleep = randint(1, MAX_TIME_SLEEP)*1000
    epoch += time_to_sleep
    #info("Sleeping for %s seconds" % time_to_sleep)
    #time.sleep(time_to_sleep)

    # sending the data
    f = tempfile.SpooledTemporaryFile()
    data = e.read()
    if COMPRESSION:
        data = compress(data)
    f.write(aes_encrypt(data, KEY))
    f.seek(0)
    e.close()

    packet_index = 0
    while (True):
        data_file = f.read(randint(MIN_BYTES_READ, MAX_BYTES_READ))
        if not data_file:
            break

        data = "%s|!|%s|!|%s" % (jobid, packet_index, data_file)
        domains += send(data, TLD, epoch, KEY, jobid, cableID, maxSize)
        packet_index = packet_index + 1

        time_to_sleep = randint(1, MAX_TIME_SLEEP)*1000
        epoch += time_to_sleep

    # last packet
    data = "%s|!|%s|!|DONE" % (jobid, packet_index)
    domains += send(data, TLD, epoch, KEY, jobid, cableID, maxSize)
    f.close()
    return(domains)

if __name__ == '__main__':
    main()
