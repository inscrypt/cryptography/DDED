from mysql import connector
import secret

'''
300mi read
Mean:           21.272925457550368
Variance:       67.40328210650885
Deviation:      8.209950189039446
2008 and 2009
'''

if __name__ == '__main__':
    database = connector.connect(
        host=secret.host,
        user=secret.user,
        passwd=secret.password,
        database=secret.database)
    mycursor = database.cursor()
    mycursor.execute("SELECT length FROM `queries-B`")
    myresult = mycursor.fetchone()
    sum = 0
    counter = 0
    while myresult:
        if counter % 1000000 == 0:
            print(counter, end='\r')
        sum += myresult[0]
        counter += 1
        myresult = mycursor.fetchone()
    mean = sum / counter

    mycursor.execute("SELECT length FROM `queries-B`")
    myresult = mycursor.fetchone()
    sum = 0
    while myresult:
        sum += (myresult[0] - mean)**2
        myresult = mycursor.fetchone()
    variance = sum / (counter - 1)
    deviation = variance**(1/2)
    print('Mean:\t\t' + str(mean))
    print('Variance:\t' + str(variance))
    print('Deviation:\t' + str(deviation))
