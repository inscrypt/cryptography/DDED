import gzip
from ipaddress import IPv4Address as IP
from os import listdir
from time import sleep
from mysql import connector
import secret
from createFields import create_fields

database = connector.connect(
  host=secret.host,
  user=secret.user,
  passwd=secret.password,
  database=secret.database)
sql = "INSERT INTO `queries-B` (id, epoch, malicious, counter, frequency, length, entropy, domain) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

databaseCursor = database.cursor()

errors = ['FAIL.TIMEOUT.in-addr.arpa', 'FAIL.SERVER-FAILURE.in-addr.arpa','FAIL.NON-AUTHORITATIVE.in-addr.arpa', 'localhost', 'gw']

up = '\x1b[1A'
cl = '\x1b[2K'
er = up + cl #up arrequest + clear line

print(er+'Generating list of downloaded files')
days = []
for file in listdir('../Datasets/'):
    if '.txt.gz' in file:
        days.append(file)
days.sort()

print(er+'Taking out already uploaded files')
with open('uploaded.txt', 'r') as uploadedFiles:
    for uploadedFile in uploadedFiles:
        days.remove(uploadedFile.split('\n')[0])

databaseCursor.execute("SELECT MAX(id) FROM `queries-B`;")
ID = databaseCursor.fetchone()[0] + 1

#day = 'dns-names.l7.20160817.txt.gz'
if not days:
    print(er+'Nothing to be done.')
    print('It seems all files had been read already.')

for day in days:
    print(er+'Uploading ' + day + ' to MariaDB.')
    fields_dict = {}
    with gzip.open('../Datasets/' + day, 'rb') as requests:
        values = []
        for ii, request in enumerate(requests):
            if (ii+1) % 10000 == 0:
                if ii < 10001:
                    print(str(ii+1) +" entries read.")
                if ii > 10000:
                    print(er+str(ii+1) +" entries read.")
                databaseCursor.executemany(sql, values)
                values = []
            if request.decode('ascii')[-1] == '\n':
                fields = request.decode('ascii')[:-1].split('\t', 2)
            else:
                fields = request.decode('ascii').split('\t', 2)

            if not fields[2] in errors:
                epoch = int(fields[0])
                malicious = 0
                domain = fields[2]
                if len(domain.split('.', 1)) > 1:
                    counter, frequency, length, entropy = create_fields(fields_dict,domain,epoch)
                    values.append((ID,epoch,malicious,counter,frequency,length,entropy,domain))
                    ID += 1
    databaseCursor.executemany(sql, values)
    database.commit()
    with open('uploaded.txt', 'a') as uploadedFiles:
        uploadedFiles.write(day + '\n')
print(er,end='')
