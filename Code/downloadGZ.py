from calendar import monthrange as mr
from urllib import request, error
import threading
import sys
from time import sleep
from random import randint as rand
from os import listdir

url = "http://data.caida.org/datasets/topology/ark/ipv4/dns-names/"
max_simultaneous = 6
wait_min = 10 #ms
wait_max = 600 #ms

threads = []
counter = 0

dirlist = listdir("./")

#http://data.caida.org/datasets/topology/ark/ipv4/dns-names/2010/06/dns-names.l7.20100601.txt.gz

def download_link(download_path, link):
	req = request.Request(link, headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0'})
	try:
		with request.urlopen(req) as file, open(download_path, 'wb') as f:
			f.write(file.read())
	except error.HTTPError as e:
		print()
		f = open('error.log', 'a')
		f.write('\n')
		if hasattr(e,'reason'):
			f.write(str(e.reason)+'\n')
			print (e.reason)
		f.write(link + '\n')
		print('URL: ' + link)
		if hasattr(e,'headers'):
			f.write(str(e.headers)+'\n')
			print (e.headers)
		f.close()

for year in range(2009, 2018+1):
	for month in range(1,12+1):
		print(str(year)+str(month).zfill(2), end='\r')
		for day in range(1,mr(year, month)[1]+1):
			file_name = "dns-names.l7." + str(year) + str(month).zfill(2) + str(day).zfill(2) + ".txt.gz"
			if not file_name in dirlist:
				counter += 1
				if counter == max_simultaneous:
					counter = 0
					for x in threads:
						x.join()
					threads = []
				completeURL = url + str(year) + '/' + str(month).zfill(2) + '/' + file_name
				thread = threading.Thread(target=download_link,args=('../Datasets/' + file_name, completeURL))
				sleep(rand(wait_min,wait_max)/1000)
				thread.start()
				threads += [thread]
