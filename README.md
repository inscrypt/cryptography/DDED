Objective:
Outlier detection to fight agains DNS data exfiltration using [DET](https://github.com/PaulSec/DET).
We are using as benign data [Caida database](https://www.caida.org/data/active/ipv4_dnsnames_dataset.xml), we have 8.720.307.258 requests from 2008 to 2018.